// ==UserScript==
// @name         OSM Links
// @namespace    https://www.openstreetmap.org/
// @version      2023.12.27
// @description  Add buttons that link to the same location on other maps
// @author       tguen
// @icon         https://icons.duckduckgo.com/ip2/openstreetmap.org.ico
// @match        https://www.openstreetmap.org/*
// @grant        none
// ==/UserScript==


(function() {
	'use strict';


	window.jQuery(document).ready(onLoad)


	function onLoad() {
		const buttonUpdaterList = []
		const menuButtons = []

		{ // common
			const div = document.createElement('div');
			div.classList.add('btn-group');
			div.style.marginLeft = '20px';
			div.appendChild(makeLink('Bing', (pos) => `https://www.bing.com/maps?lvl=${pos.zoom}.0&cp=${pos.lat}~${pos.lon}&style=h`))
			div.appendChild(makeLink('PortlandMaps', (pos) => {
				const posEpsg3857 = epsg4326to3857(pos)
				return `https://www.portlandmaps.com/detail/property/${posEpsg3857.lon}_${posEpsg3857.lat}_xy/`
			}))
			document.querySelector('nav.primary').appendChild(div);
		}

		addToolbarMenu('Maps', [
			{ text:'Bing', getLink:(pos) => `https://www.bing.com/maps?lvl=${pos.zoom}.0&cp=${pos.lat}~${pos.lon}&style=h` },
			{ text:'GMaps', getLink:(pos) => `https://www.google.com/maps/@${pos.lat},${pos.lon},${pos.zoom}z/data=!3m1!1e3` },
			{ text:'GEarth', getLink:(pos) => `https://earth.google.com/web/@${pos.lat},${pos.lon},4000d` },
			{ text:'Mapillary', getLink:(pos) => `https://www.mapillary.com/app/?lat=${pos.lat}&lng=${pos.lon}&z=${pos.zoom}` },
			{ text:'Streets.gl', getLink:(pos) => `https://streets.gl/#${pos.lat},${pos.lon},90,0,400` },
			{ text:'F4', getLink:(pos) => `https://demo.f4map.com/#lat=${pos.lat}&lon=${pos.lon}&zoom=${pos.zoom}` },
			{ text:'qa.poole.ch', getLink:(pos) => `https://qa.poole.ch/?zoom=${pos.zoom}&lat=${pos.lat}&lon=${pos.lon}` },
			{ text:'Overpass', getLink:(pos) => `https://tyrasd.github.io/overpass-turbo/#C=${pos.lat};${pos.lon};${pos.zoom}` },
		])

		addToolbarMenu('GIS', [
			{ text:'PortlandMaps', getLink:(pos) => {
				const posEpsg3857 = epsg4326to3857(pos)
				return `https://www.portlandmaps.com/detail/property/${posEpsg3857.lon}_${posEpsg3857.lat}_xy/`
			}},
			{ text:'Metro Addr', getLink:(pos) => `https://rlisdiscovery.oregonmetro.gov/datasets/master-address-file-maf-1/explore?location=${pos.lat},${pos.lon},${pos.zoom-1}` },
			{ text:'Metro Lots', getLink:(pos) => `https://rlisdiscovery.oregonmetro.gov/datasets/drcMetro::taxlots-public/explore?location=${pos.lat},${pos.lon},${pos.zoom-1}` },
			{ text:'Metro Streets', getLink:(pos) => `https://rlisdiscovery.oregonmetro.gov/datasets/drcMetro::streets-2/explore?location=${pos.lat},${pos.lon},${pos.zoom-1}` },

			{ text:'Clark Addr', getLink:(pos) => `https://hub-clarkcountywa.opendata.arcgis.com/datasets/situs-addresses/explore?location=${pos.lat},${pos.lon},${pos.zoom-1}` },
			{ text:'Clark Lots', getLink:(pos) => `https://hub-clarkcountywa.opendata.arcgis.com/datasets/taxlots-for-public-use/explore?location=${pos.lat},${pos.lon},${pos.zoom-1}` },
			{ text:'Clark Streets', getLink:(pos) => `https://hub-clarkcountywa.opendata.arcgis.com/datasets/ClarkCountyWa::road-centerlines/explore?location=${pos.lat},${pos.lon},${pos.zoom-1}` },
		])

		document.getElementById('map').addEventListener('click', closeAll)

		updateLinks()
		const observer = new MutationObserver(function(mutationList, observer) {
			for (const mutation of mutationList) {
				if (mutation.oldValue !== mutation.target.href) {
					updateLinks()
				}
			}
		})
		observer.observe(document.getElementById('editanchor'), { attributes: true, attributeFilter: [ 'href' ] })


		function addToolbarMenu(name, links) {
			const div = document.createElement('div')
			div.classList.add('btn-group')
			div.style.marginLeft = '20px'

			const ul = document.createElement('ul')
			ul.classList.add('dropdown-menu')
			ul.style = 'position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(0, 37px);'
			for (let link of links) {
				const li = document.createElement('li')
				const a = document.createElement('a')
				a.classList.add('dropdown-item')
				a.textContent = link.text
				buttonUpdaterList.push({
					btn: a,
					update: link.getLink
				})
				li.appendChild(a)
				ul.appendChild(li)
			}

			const menuExpander = makeDropMenuToggle(name, ul)
			menuButtons.push({ menu: ul, button: menuExpander })
			div.appendChild(menuExpander)
			div.appendChild(ul)
			document.querySelector('nav.primary').appendChild(div)
		}

		function makeDropMenuToggle(text, menu) {
			const button = document.createElement('button')
			button.classList.add('btn', 'btn-outline-primary', 'dropdown-toggle')
			button.textContent = text
			button.onclick = function() {
				if (!menu.classList.contains('show')) {
					closeAll()
					menu.classList.add('show')
					button.classList.add('show')
				}
				else closeAll()
			}
			return button
		}

		function makeLink(text, getLink) {
			const button = document.createElement('a')
			button.classList.add('btn', 'btn-outline-primary')
			button.textContent = text
			buttonUpdaterList.push({
				btn: button,
				update: getLink
			})
			return button
		}

		function closeAll() {
			for (let menuToggle of menuButtons) close(menuToggle)
		}

		function close(menuToggle) {
			if (menuToggle.menu.classList.contains('show')) {
				menuToggle.menu.classList.remove('show')
				menuToggle.button.classList.remove('show')
			}
		}

		function epsg4326to3857(pos) {
			// https://gis.stackexchange.com/questions/142866/converting-latitude-longitude-epsg4326-into-epsg3857
			const smRadius = 6378136.98
			const smRange = smRadius * Math.PI * 2.0
			const smRadiansOverDegrees = Math.PI / 180.0

			const lon2 = pos.lon * smRange / 360.0
			let lat2
			if (pos.lat > 86.0) lat2 = smRange
			else if (pos.lat < -86.0) lat2 = -smRange
			else {
				let y = pos.lat * smRadiansOverDegrees
				y = Math.log(Math.tan(y) + (1.0 / Math.cos(y)), Math.E)
				lat2 = y * smRadius
			}

			return {
				zoom: pos.zoom,
				lat: lat2,
				lon: lon2,
			}
		}

		function getPos() {
			const editUrl = document.getElementById('editanchor').href
			const findArgs = editUrl.match(/#map=(\d+)\/(-?\d+(?:\.\d+)?)\/(-?\d+(?:\.\d+)?)/)

			//console.log(findArgs)
			if (!findArgs) {
				console.error(`could not find map location in url: ${editUrl}`)
				return
			}

			return {
				zoom: findArgs[1],
				lat: findArgs[2],
				lon: findArgs[3],
			}
		}

		function updateLinks() {
			const pos = getPos();
			for (let btnUpdater of buttonUpdaterList) {
				btnUpdater.btn.href = btnUpdater.update(pos)
			}
		}
	}

})();
