// ==UserScript==
// @name         OSM Sidebar & Slippy Buttons
// @namespace    https://www.openstreetmap.org/
// @version      2023.11.07
// @description  Add links to changesets, replace timestamps, add buttons to slippy map
// @author       tguen
// @icon         https://icons.duckduckgo.com/ip2/openstreetmap.org.ico
// @match        https://www.openstreetmap.org/*
// @grant        none
// ==/UserScript==


(function() {
	'use strict';


	window.jQuery(document).ready(onLoad)


	function onLoad() {
		updateSidebarContent()
		observeSidebar()
		addSideButtons()
	}


	function observeSidebar() {
		const observer = new MutationObserver(updateSidebarContent)
		observer.observe(document.getElementById('sidebar_content'), { childList: true })
		document.getElementById('sidebar').setAttribute('observed', true)
	}

	function updateSidebarContent() {
		const sidebar = document.getElementById('sidebar_content')
		if (sidebar.style.display === 'hidden') {
			return
		}

		updateTimes(sidebar)
		makeChangesetButtons(sidebar)


		// force times to display as local time zone
		function updateTimes(sidebar) {
			doUpdate(sidebar)

			// add observer for user changesets loaded
			if (window.location.href.match(/www.openstreetmap.org\/user\/([^\/]+)\/history/)) {
				const element = sidebar.children[1]
				if (!element || element.getAttribute('observe-timestamps')) {
					return
				}
				element.setAttribute('observe-timestamps', true)

				new MutationObserver(function() {
					doUpdate(sidebar)
				}).observe(element, { childList: true })
			}


			function doUpdate(sidebar) {
				for (let abbr of sidebar.querySelectorAll('abbr')) {
					//console.log(abbr)
					if (!abbr.title) {
						continue
					}

					let time = Date.parse(abbr.title)
					if (!isNaN(time)) {
						const span = abbr.children[0] ? abbr.children[0] : abbr
						span.title = formatDate(new Date(abbr.title))
					}
					else {
						const match = abbr.title.match(/Created: (.+)\nClosed: (.+)/)
						if (match) {
							const created = new Date(match[1])
							const closed = new Date(match[2])

							abbr.title = `Created: ${formatDate(created)}\nClosed: ${formatDate(closed)}`
						}
						else {
							console.warn(`failed to format as date: ${abbr.title}`)
						}
					}
				}
			}

			function formatDate(date) {
				const year = date.getFullYear()
				const month = asTwoDigit(date.getMonth()+1)
				const day = asTwoDigit(date.getDate())
				const hour = asTwoDigit(date.getHours())
				const min = asTwoDigit(date.getMinutes())
				const sec = asTwoDigit(date.getSeconds())
				return `${year}-${month}-${day} – ${hour}:${min}:${sec}`


				function asTwoDigit(num) {
					return num > 9 ? `${num}` : `0${num}`
				}
			}
		}

		function makeChangesetButtons(sidebar) {
			let changesetUrlMatch = window.location.href.match(/www.openstreetmap.org\/changeset\/(\d+)/)

			// changeset page
			if (changesetUrlMatch) {
				for (let header of sidebar.querySelectorAll('h2')) {
					if (header.textContent.match(/^Changeset:/)) {
						const div = sidebar.querySelector('.browse-section')
						if (div === null) {
							return
						}
						div.insertAdjacentHTML('afterbegin', `
						<div>
						${osmlabLinkHtml(changesetUrlMatch[1])}
						${achaviLinkHtml(changesetUrlMatch[1])}
						</div>
					`)
					}
				}
			}
			// feature page
			else if (window.location.href.match(/www.openstreetmap.org\/(node|way|relation)/)) {
				for (let section of sidebar.querySelectorAll('.browse-section')) {
					for (let link of section.querySelectorAll('a')) {
						changesetUrlMatch = link.href.match(/\/changeset\/(\d+)$/)
						if (!changesetUrlMatch) {
							continue
						}
						link.parentElement.parentElement.insertAdjacentHTML('beforeend',
							`<li>${osmlabLinkHtml(changesetUrlMatch[1])}${achaviLinkHtml(changesetUrlMatch[1])}</li>`
						)
						break
					}
				}
			}
			// user history page
			else if (window.location.href.match(/www.openstreetmap.org\/user\/([^\/]+)\/history/)) {
				const element = sidebar.children[1]
				if (!element || element.getAttribute('observe-changeset-links')) {
					return
				}
				element.setAttribute('observe-changeset-links', true)

				new MutationObserver(function() {
					for (let element of sidebar.querySelectorAll('ol.changesets > li')) {
						if (element.getAttribute('changeset-links')) {
							continue
						}
						element.setAttribute('changeset-links', true)

						const link = element.querySelector('a.changeset_id')
						changesetUrlMatch = link.href.match(/\/changeset\/(\d+)$/)
						if (!changesetUrlMatch) {
							continue
						}
						element.insertAdjacentHTML('beforeend', `
							<div>
							${osmlabLinkHtml(changesetUrlMatch[1])}
							${achaviLinkHtml(changesetUrlMatch[1])}
							</div>
						`)
					}
				}).observe(element, { childList: true })
			}


			function linkHtml(text, url) {
				return `<a class="btn btn-sm action-btn btn-outline-primary" href="${url}" style="margin-right: 12px; padding: 0 0.25rem;">${text}</a>`
			}

			function osmlabLinkHtml(changeset) {
				return linkHtml(`OSM Lab`, `https://osmlab.github.io/changeset-map/#${changeset}`)
			}

			function achaviLinkHtml(changeset) {
				return linkHtml(`Achavi`, `https://overpass-api.de/achavi/?changeset=${changeset}`)
			}
		}
	}

	function addSideButtons() {
		const queryButton = document.getElementById('map').querySelector('.control-query')
		const addNoteButton = document.getElementById('map').querySelector('.control-note')
		const buttonGroup = queryButton.parentElement

		const uiSidebar = document.getElementById('map-ui')
		const layers = uiSidebar.querySelector('.overlay-layers')

		buttonGroup.insertBefore(makeButton(
			['control-button', 'control-button-first', 'control-button-last'],
			function() {
				const header = document.body.querySelector('header')
				const content = document.getElementById('content')
				const search = document.getElementById('sidebar').querySelector('.search_forms')

				if (header.getAttribute('tm-hidden') == 'true') {
					header.setAttribute('tm-hidden', false)

					header.style.display = ''
					content.style.top = '55px'
					search.style.display = ''
					for (let sideButton of buttonGroup.children) {
						if (sideButton != buttonGroup.children[0]) {
							sideButton.style.display = ''
						}
					}
				}
				else {
					header.setAttribute('tm-hidden', true)

					header.style.display = 'none'
					content.style.top = '0'
					search.style.display = 'none'
					for (let sideButton of buttonGroup.children) {
						if (sideButton != buttonGroup.children[0]) {
							sideButton.style.display = 'none'
						}
					}
				}
			}
		), buttonGroup.children[0])

		let noteCheckbox = null
		let dataCheckbox = null
		for (let label of layers.querySelectorAll('label.form-check-label')) {
			if (label.textContent === 'Map Notes') {
				noteCheckbox = label.querySelector('input')
			}
			if (label.textContent === 'Map Data') {
				dataCheckbox = label.querySelector('input')
			}
		}

		if (noteCheckbox !== null) {
			addNoteButton.querySelector('a').classList.remove('control-button-first')
			const newButton = makeButton( ['control-button', 'control-button-first'], function() { noteCheckbox.click(); })
			buttonGroup.insertBefore(newButton, addNoteButton)

			const a = newButton.querySelector('a')
			updateButtonActive(a, noteCheckbox)
			noteCheckbox.addEventListener('change', function() { updateButtonActive(a, noteCheckbox); })
		}

		if (dataCheckbox !== null) {
			queryButton.querySelector('a').classList.remove('control-button-first')
			const newButton = makeButton(['control-button', 'control-button-first'], function() { dataCheckbox.click(); })
			buttonGroup.insertBefore(newButton, queryButton)

			const a = newButton.querySelector('a')
			a.id = 'show-data-btn'
			updateButtonActive(a, dataCheckbox)
			dataCheckbox.addEventListener('change', function() { updateButtonActive(a, dataCheckbox); })

			if (!dataCheckbox.getAttribute('observed')) {
				dataCheckbox.setAttribute('observed', true)

				const editButton = document.getElementById('editanchor')
				const observer = new MutationObserver(function(mutationList, observer) {
					for (const mutation of mutationList) {
						if (mutation.oldValue !== mutation.target.href) {
							updateDataButton()
						}
					}
				})
				observer.observe(editButton, { attributes: true, attributeFilter: [ 'href' ] })

				updateDataButton()


				function updateDataButton() {
					const a = document.getElementById('show-data-btn')
					const match = editButton.href.match(/edit#map=([0-9]+)/)

					if (match !== null && parseInt(match[1]) < 18) {
						a.classList.add('disabled')
						if (a.classList.contains('active')) {
							a.click()
						}
					}
					else {
						a.classList.remove('disabled')
					}
				}
			}
		}


		function makeButton(linkClassList, onClick) {
			const newButton = document.createElement('div')
			newButton.classList.add('leaflet-control')

			const a = document.createElement('a')
			a.classList.add(...linkClassList)
			a.addEventListener('click', onClick)
			newButton.appendChild(a)
			return newButton
		}

		function updateButtonActive(btn, checkbox) {
			if (checkbox.checked) {
				btn.classList.add('active')
			}
			else {
				btn.classList.remove('active')
			}
		}

		function onDataDisabled() {
			const li = dataCheckbox.parentElement.parentElement
			const a = document.getElementById('show-data-btn')
			console.log(a)

			if (li.classList.contains('disabled')) {
				a.classList.add('disabled')
			}
			else {
				a.classList.remove('disabled')
			}
		}
	}

})();
